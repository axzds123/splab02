objects = main.o empmgr.o readfile.o
all : $(objects)
	gcc -o workDB $(objects)
main.o : main.c
	gcc -c main.c
empmgr.o : empmgr.c empmgr.h
	gcc -c empmgr.c
readfile.o : readfile.c readfile.h
	gcc -c readfile.c
clean :
	rm workDB $(objects)