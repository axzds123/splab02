//
// Created by Zombie on 2020/11/4.
//

#include <stdio.h>
#include <stdlib.h>

static FILE* g_fp = NULL;

int open_file(char* path)
{
    if (g_fp != NULL)
        return -2;

    g_fp = fopen(path , "r");
    if (g_fp == NULL)
        return -1;

    return 0;
}

int write_and_flush_file(char* path ,char* data, int length)
{
    FILE *fp = fopen(path, "w");
    if(fp == NULL)
        return -1;

    fwrite(data, length , 1, fp);

    fclose(fp);
    return 0;
}

void close_file()
{
    fclose(g_fp);
    g_fp = NULL;
}

int read_int(int* a)
{
    if (fscanf(g_fp, "%d", a) != EOF)
        return 0;
    return -1;
}

int read_float(float* a)
{
    if (fscanf(g_fp, "%f", a) != EOF)
        return 0;
    return -1;
}

int read_string(char* a)
{
    if (fscanf(g_fp, "%s", a) != EOF)
        return 0;
    return -1;

}