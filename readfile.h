//
// Created by Zombie on 2020/11/4.
//

#ifndef SPLAB1_READFILE_H
#define SPLAB1_READFILE_H

#endif //SPLAB1_READFILE_H

int open_file(char* path);
void close_file();


int read_float(float* a);
int read_int(int* a);
int read_string(char* a);
int write_and_flush_file(char* path ,char* data, int length);